cdef struct ConfigurationType:
    int a
    float b
    int c
    float d


cdef extern void __example_ml_MOD_example_subroutine(float*, float*)
cdef extern float __example_ml_MOD_example_function(float*)
cdef extern ConfigurationType __example_ml_MOD_default_struct()
cdef extern void __example_ml_MOD_modify_struct(ConfigurationType*)


def test():
    cdef float a = 12.0
    cdef float b = 0.0
    __example_ml_MOD_example_subroutine(&a, &b)
    print(a, b)

    b = __example_ml_MOD_example_function(&a)
    print(a, b)

    cdef ConfigurationType c
    c.a = 12
    c.b = 15.0
    c.c = 0
    c.d = 123.0
    __example_ml_MOD_modify_struct(&c)
    print(c)

    c = __example_ml_MOD_default_struct()
    print(c)
