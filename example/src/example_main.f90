program example_main

    use example_ml

    real :: a, b

    a = 12.0
    call example_subroutine(a, b)

    print *, a, b

end program example_main
