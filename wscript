#!/usr/bin/env python3

import sys
import os

from waflib.Configure import conf


@conf
def find_win32_python_script(conf, filename, var=None):
    if not conf.env.PYTHON:
        conf.load('python')

    if sys.platform != 'win32':
        conf.fatal('Only use find_win32_python_script on win32')

    var = var or filename.upper()
    assert conf.env.PYTHON[0].endswith('python.exe')
    script = os.path.join(os.path.dirname(conf.env.PYTHON[0]),
                          'Scripts', filename + '.py')
    if not os.path.isfile(script):
        conf.fatal('Could not find the python script {}.py'.format(filename))
    conf.env[var] = conf.env.PYTHON + [script]


def options(opt):
    opt.recurse('example')

    opt.load('compiler_c')
    opt.load('python')
    opt.load('cython')


def configure(conf):
    conf.recurse('example')

    conf.load('compiler_c')
    conf.load('python')

    conf.check_python_version((3, 3, 0))
    conf.check_python_headers()

    # Python scripts live somewhere else on win32, find_program won't find cython
    if sys.platform == 'win32':
        conf.find_win32_python_script('cython')
    conf.load('cython')

    if conf.env.FC_NAME == 'GFORTRAN':
        conf.env.LIB_FORTRAN = 'gfortran'
    else:
        conf.fatal('No handling for Fortran compilers other than gfortran')


def build(bld):
    bld.recurse('example')
    bld(features='c cshlib pyext',
        source='example_interface.pyx',
        use='example_modules FORTRAN',
        target='example_interface')
